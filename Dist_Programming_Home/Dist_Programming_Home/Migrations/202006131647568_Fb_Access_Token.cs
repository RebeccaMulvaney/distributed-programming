namespace Dist_Programming_Home.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Fb_Access_Token : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "Fb_Access_Token", c => c.String());
            AddColumn("dbo.AspNetUsers", "Tw_Access_Token", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "Tw_Access_Token");
            DropColumn("dbo.AspNetUsers", "Fb_Access_Token");
        }
    }
}
