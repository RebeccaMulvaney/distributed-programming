﻿using System.Web;
using System.Web.Mvc;

namespace Dist_Programming_Home
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
